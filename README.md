## Candidate Evaluation

### Overview
This project provides a solution to the applicants AMR example.
It is developed explicitly for ROS2 Foxy Fitzroy and Ubuntu 20.04.
A simple action server handles requests to parse yaml files containing order definitions.
The OrderOptimizer node takes an absolute file path as argument, parses the yaml file
containing product configurations and calculates a path to collect all necessary
parts given an order.
The project contains two packages, order_optimizer with the implementation of the action
server and the OrderOptimizer node and the order_optimizer_interface package with the projects
own interfaces.
Communication with the node is as specified in the problem definition. The PDF can be found
in the sample_files/ directory.

### Dependencies
The dependencies are kept to a minimum. No additional downloads should be required.
- rclcpp
- yaml-cpp
- std_msgs
- geometry_msgs
- visualization_msgs

### Usage
The OrderOptimizer package contains a launch file that can be used to launch the node and the action server.
Note that you need to update the file path parameter that is specified in this launch file
as it specifies an absolute path!
The packages are built using 'colcon build'.
You can set the current robot position via the /current_position topic.
New orders are sent to the OrderOptimizer node via the /next_order topic.

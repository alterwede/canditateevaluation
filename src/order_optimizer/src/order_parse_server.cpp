#include <thread>
#include <fstream>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include <yaml-cpp/yaml.h>

#include "order_optimizer_interfaces/action/order_parse.hpp"
#include "order_optimizer_interfaces/msg/order.hpp"


class OrderParseServer : public rclcpp::Node
{
public:
	using OrderParse = order_optimizer_interfaces::action::OrderParse;
	using GoalHandleOrderParse = rclcpp_action::ServerGoalHandle<OrderParse>;

	explicit OrderParseServer(const rclcpp::NodeOptions & options = rclcpp::NodeOptions())
	: Node("order_parse_server", options)
	{
        using namespace std::placeholders;

	    this->action_server_ = rclcpp_action::create_server<OrderParse>(
            this, "order_parse",
            std::bind(&OrderParseServer::handle_goal, this, _1, _2),
            std::bind(&OrderParseServer::handle_cancel, this, _1),
            std::bind(&OrderParseServer::handle_accepted, this, _1));
	}

private:

    rclcpp_action::GoalResponse handle_goal(
        const rclcpp_action::GoalUUID & uuid,
        std::shared_ptr<const OrderParse::Goal> goal)
    {
    RCLCPP_INFO(this->get_logger(), "Received goal request for file " + goal->order_file_path);
        (void)uuid;
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse handle_cancel(
        const std::shared_ptr<GoalHandleOrderParse> goal_handle)
    {
        RCLCPP_INFO(this->get_logger(), "Received request to cancel goal");
        (void)goal_handle;
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void handle_accepted(const std::shared_ptr<GoalHandleOrderParse> goal_handle)
    {
        using namespace std::placeholders;
        std::thread{std::bind(&OrderParseServer::execute, this, _1), goal_handle}.detach();
    }

    void execute(const std::shared_ptr<GoalHandleOrderParse> goal_handle)
    {
        try {
            RCLCPP_INFO(this->get_logger(), "Executing goal");
            const auto goal = goal_handle->get_goal();
            auto feedback = std::make_shared<OrderParse::Feedback>();
            auto & orders = feedback->partial_orders;
            auto result = std::make_shared<OrderParse::Result>();
            std::ifstream fin(goal->order_file_path);
            YAML::Node yaml_node = YAML::Load(fin);
            for (unsigned i=0; i<yaml_node.size(); i++) {
                RCLCPP_DEBUG(this->get_logger(), "         reading next node.");
                if (goal_handle->is_canceling()) {
                    result->orders = orders;
                    goal_handle->canceled(result);
                    RCLCPP_INFO(this->get_logger(), "Goal canceled");
                    return;
                }
                auto order_msg = create_msg_from_yaml_node(yaml_node[i]);
                orders.push_back(order_msg);
            }
            if (rclcpp::ok()) {
              result->orders = orders;
              goal_handle->succeed(result);
              RCLCPP_INFO(this->get_logger(), "Goal succeeded");
            }
        } catch (...) {
            result->orders = orders;
            goal_handle->canceled(result);
            RCLCPP_ERROR(this->get_logger(), "Error executing goal, make sure the requested file is valid. Goal canceled.");
            return;
        }
    }

    order_optimizer_interfaces::msg::Order create_msg_from_yaml_node(const YAML::Node& node)
    {
        auto order_msg = order_optimizer_interfaces::msg::Order();
        order_msg.order_id = node["order"].as<long>();
        order_msg.cx = node["cx"].as<float>();
        order_msg.cy = node["cy"].as<float>();
        order_msg.products = node["products"].as<std::vector<int>>();
        return order_msg;
    }

    rclcpp_action::Server<OrderParse>::SharedPtr action_server_;
};

int main(int argc, char ** argv)
{
    rclcpp::init(argc, argv);
    auto action_server = std::make_shared<OrderParseServer>();
    rclcpp::spin(action_server);
    rclcpp::shutdown();
    return 0;
}

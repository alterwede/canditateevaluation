#include <fstream>
#include <dirent.h>
#include <cmath>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include <yaml-cpp/yaml.h>

#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "visualization_msgs/msg/marker_array.hpp"
#include "visualization_msgs/msg/marker.hpp"

#include "order_optimizer_interfaces/msg/order.hpp"
#include "order_optimizer_interfaces/msg/next_order.hpp"
#include "order_optimizer_interfaces/action/order_parse.hpp"


using std::placeholders::_1;
using std::placeholders::_2;

class OrderOptimizer : public rclcpp::Node
{
public:
	using OrderParse = order_optimizer_interfaces::action::OrderParse;
	using GoalHandleOrderParse = rclcpp_action::ClientGoalHandle<OrderParse>;

	OrderOptimizer()
	: Node("order_optimizer")
	{
		RCLCPP_INFO(this->get_logger(), "OrderOptimizer starting initializaton.");
		this->declare_parameter<std::string>("file_path", "default_file_path");
		if (!(this->get_parameter("file_path", file_path_))) {
			throw std::invalid_argument("Please provide a valid absolute file path as argument");
		}
		try {
			RCLCPP_DEBUG(this->get_logger(), "Got '" + file_path_ + "' as argument");
			this->parse_configurations();
			RCLCPP_DEBUG(this->get_logger(), "I successfully parsed the configuration file. It contains " + std::to_string(products_.size()) + " products.");
		} catch (...) {
			RCLCPP_ERROR(this->get_logger(), "Error parsing the configuration files.");
			throw std::invalid_argument("Please provide a valid absolute file path as argument and make sure the yaml file follows the template.");
		}
		publisher_ = this->create_publisher<visualization_msgs::msg::MarkerArray>("order_path", 10);
		next_order_subscription_ = this->create_subscription<order_optimizer_interfaces::msg::NextOrder>(
				"next_order", 10, std::bind(&OrderOptimizer::next_order_callback, this, _1));
		current_position_subscription_ = this->create_subscription<geometry_msgs::msg::PoseStamped>(
				"current_position", 10, std::bind(&OrderOptimizer::current_position_callback, this, _1));
	    this->client_ptr_ = rclcpp_action::create_client<OrderParse>(
	      this,
	      "order_parse");
	}

struct Part {
	std::string part;
	int product_id;
	float cx;
	float cy;

	void operator()(const YAML::Node& node) {
		part = node["part"].as<std::string>();
		cx = node["cx"].as<float>();
		cy = node["cy"].as<float>();
	}
	void operator()(order_optimizer_interfaces::msg::Order order) {
		cx = order.cx;
		cy = order.cy;
	}
	void operator()(geometry_msgs::msg::PoseStamped pose_stamped) {
		cx = pose_stamped.pose.position.x;
		cy = pose_stamped.pose.position.y;
	}
	float operator-(Part part) {
		return std::sqrt(
				std::pow((cx-part.cx), 2)
				+ std::pow((cy-part.cy), 2)
				);
	}
};

struct Product {
	int id;
	std::string product;
	std::vector <Part> parts;

	void operator()(const YAML::Node& node) {
		id = std::stoi(node["id"].as<std::string>());
		product = node["product"].as<std::string>();
		const YAML::Node& parts_node = node["parts"];
		for (unsigned i=0; i<parts_node.size(); i++) {
			Part part;
			part(parts_node[i]);
			parts.push_back(part);
		}
	}
};

private:
	void next_order_callback(const order_optimizer_interfaces::msg::NextOrder::SharedPtr msg)
	{
		RCLCPP_INFO(this->get_logger(), "I received next order: '%s'", msg->description.c_str());
		next_order_ = *msg;
		orders_.clear();
		std::vector<std::string> file_names;
		this->find_yaml_in_directory(file_path_ + ORDER_PATH_APPENDIX, file_names);
		outstanding_responses_ = 0;
		for (const auto & file_name : file_names) {
			RCLCPP_DEBUG(this->get_logger(), "Trying to send goal: " + file_name);
			this->send_goal(file_name);
			outstanding_responses_++;
		}
    }

	void current_position_callback(const geometry_msgs::msg::PoseStamped::SharedPtr msg)
	{
		std::string x = std::to_string(msg->pose.position.x);
		std::string y = std::to_string(msg->pose.position.y);
		std::string z = std::to_string(msg->pose.position.z);
		RCLCPP_DEBUG(this->get_logger(), "Received new current position: x=" + x + ", y=" + y + ", z=" + z);
		current_position_ = *msg;
	}

	void send_goal(std::string order_file_name) {
	    using namespace std::placeholders;

	    if (!this->client_ptr_->wait_for_action_server()) {
	      RCLCPP_ERROR(this->get_logger(), "Action server not available after waiting");
	    }

	    auto goal_msg = OrderParse::Goal();
	    goal_msg.order_file_path = order_file_name;

	    auto send_goal_options = this->create_goal_options();
	    this->client_ptr_->async_send_goal(goal_msg, send_goal_options);
	    RCLCPP_DEBUG(this->get_logger(), "Sent goal " + goal_msg.order_file_path);
	}

	void goal_response_callback(std::shared_future<GoalHandleOrderParse::SharedPtr> future)
	{
		auto goal_handle = future.get();
		if (!goal_handle) {
			RCLCPP_ERROR(this->get_logger(), "Goal was rejected by server");
		} else {
			RCLCPP_DEBUG(this->get_logger(), "Goal accepted by server, waiting for result");
	    }
	}

	void feedback_callback(GoalHandleOrderParse::SharedPtr,
			const std::shared_ptr<const OrderParse::Feedback> feedback)
	{
	}

	void result_callback(const GoalHandleOrderParse::WrappedResult & result)
	{
		switch (result.code) {
			case rclcpp_action::ResultCode::SUCCEEDED:
				break;
			case rclcpp_action::ResultCode::ABORTED:
				RCLCPP_ERROR(this->get_logger(), "Goal was aborted");
				return;
			case rclcpp_action::ResultCode::CANCELED:
				RCLCPP_ERROR(this->get_logger(), "Goal was canceled");
				return;
			default:
				RCLCPP_ERROR(this->get_logger(), "Unknown result code");
				return;
	    }
		const std::lock_guard<std::mutex> lock(orders_mutex_);
		for (auto order : result.result->orders) {
			orders_.push_back(order);
		}
		outstanding_responses_--;
		RCLCPP_DEBUG(this->get_logger(), "Added parsed order results to orders. Number of outstanding responses: '%d'", outstanding_responses_);
		if (outstanding_responses_ == 0) {
			this->all_responses_received_callback();
		}
	  }

	void all_responses_received_callback()
	{
		// We ignore the possibility of duplicate order definitions for now.
		try {
			order_optimizer_interfaces::msg::Order order = get_order_definition();
			RCLCPP_INFO(this->get_logger(), "Working on order " + std::to_string(next_order_.order_id) + " (" + next_order_.description + ")");
			std::vector<Part> needed_parts = this->get_needed_parts(order);
			Part start;
			start(current_position_);
			Part goal;
			goal(order);
			std::vector<Part> path = this->plan_path(start, goal, needed_parts);
			for (unsigned int i = 1; i < path.size() - 1; i++) {
				RCLCPP_INFO(this->get_logger(), "Fetching part '" + path[i].part + "' for product '" + std::to_string(path[i].product_id) + "' at x: " + std::to_string(path[i].cx) + ", y: " + std::to_string(path[i].cy));
			}
			RCLCPP_INFO(this->get_logger(), "Delivering to destination x: " + std::to_string(path.back().cx) + ", y: " + std::to_string(path.back().cy));
			auto message = visualization_msgs::msg::MarkerArray();
			this->fill_visualization_message(path, message);
			publisher_->publish(message);
		} catch (std::domain_error& e) {
			RCLCPP_ERROR(this->get_logger(), "The requested next_order could not be processed due to missing definitions in the yaml files.");
			return;
		}
	}

	void parse_configurations()
	{
		if (file_path_.compare("default_file_path") == 0) {
			throw std::runtime_error("Not good...");
		}
		std::ifstream fin(file_path_ + CONFIG_PATH_APPENDIX);
		YAML::Node yaml_node = YAML::Load(fin);
		for (unsigned int i=0; i<yaml_node.size(); i++) {
			Product product;
			product(yaml_node[i]);
			products_.push_back(product);
		}
	}

	std::vector<Part> plan_path(const Part start, const Part goal, const std::vector<Part> nodes) {
		// Not guaranteed to find shortest path.
		std::vector<Part> unvisited_set = nodes;
		std::vector<float> distances;
		std::vector<Part> path;
		path.push_back(start);
		Part current_node = start;
		while (unvisited_set.size() > 0) {
			for (auto node : unvisited_set) {
				distances.push_back(node-current_node);
			}
			int min_element_index = std::min_element(distances.begin(), distances.end()) - distances.begin();
			current_node = unvisited_set[min_element_index];
			path.push_back(current_node);
			unvisited_set.erase(unvisited_set.begin() + min_element_index);
			distances.clear();
		}
		path.push_back(goal);
		return path;
	}

	order_optimizer_interfaces::msg::Order get_order_definition()
	{
		for (auto order : orders_) {
			if (order.order_id == next_order_.order_id) {
				RCLCPP_DEBUG(this->get_logger(), "Found definition of order with id: '%d'", order.order_id);
				return order;
			}
		}
		throw std::domain_error("The requested next_order is not defined in the order yaml files.");
	}

	std::vector<Part> get_needed_parts(order_optimizer_interfaces::msg::Order order)
	{
		std::vector<Part> needed_parts;
		for (auto product_id : order.products) {
			RCLCPP_DEBUG(this->get_logger(), "Searching for definition of product with id: '%d'", product_id);
			auto pred = [&product_id](Product product) { return product_id == product.id; };
			std::vector<Product>::iterator it = std::find_if(products_.begin(), products_.end(), pred);
			if (it == products_.end()) {
				throw std::domain_error("The product with product_id " + std::to_string(product_id) + " is not defined in the configuration yaml files.");
				break;
			}
			RCLCPP_DEBUG(this->get_logger(), "Found definition of product with id: '%d'", product_id);
			for (auto part : it->parts) {
				part.product_id = product_id;
				needed_parts.push_back(part);
			}
		}
		return needed_parts;
	}

	void find_yaml_in_directory(std::string directory_path, std::vector<std::string> &v)
	{
		// uses POSIX specific functions, adapt here if you want to migrate to other platforms
		RCLCPP_DEBUG(this->get_logger(), "Searching all files in dir: " + directory_path);
		DIR* dirp = opendir(directory_path.c_str());
		struct dirent * dp;
		while ((dp = readdir(dirp)) != NULL) {
			std::string d_name (dp->d_name);
			RCLCPP_DEBUG(this->get_logger(), "Next file in dir is: " + d_name);
			if (this->has_yaml_extension(d_name)) {
				RCLCPP_DEBUG(this->get_logger(), "Adding file.");
				v.push_back(directory_path + d_name);
			}
		}
		RCLCPP_DEBUG(this->get_logger(), "Done searching files in dir: " + directory_path);
		closedir(dirp);
	}

	bool has_yaml_extension(std::string file_name)
	{
		return file_name.find(".yaml") != std::string::npos;
	}

	rclcpp_action::Client<OrderParse>::SendGoalOptions create_goal_options()
	{
	    auto send_goal_options = rclcpp_action::Client<OrderParse>::SendGoalOptions();
	    send_goal_options.goal_response_callback =
	    		std::bind(&OrderOptimizer::goal_response_callback, this, _1);
	    send_goal_options.feedback_callback =
	    		std::bind(&OrderOptimizer::feedback_callback, this, _1, _2);
	    send_goal_options.result_callback =
	    		std::bind(&OrderOptimizer::result_callback, this, _1);
	    return send_goal_options;
	}


	void fill_visualization_message(std::vector<Part> path, visualization_msgs::msg::MarkerArray& ma)
	{
		ma.markers.push_back(get_marker_for_starting_position(path.front()));
		for (unsigned int i = 1; i < path.size() - 1; i++) {
			ma.markers.push_back(get_marker_from_part(path[i]));
		}
		ma.markers.push_back(get_goal_marker(path.back()));
	}

	visualization_msgs::msg::Marker get_marker_for_starting_position(Part part)
	{
		auto marker = visualization_msgs::msg::Marker();
		marker.header.stamp = rclcpp::Time();
		marker.pose.position.x = part.cx;
		marker.pose.position.y = part.cy;
		marker.scale.x = 1;
		marker.scale.y = 1;
		marker.color.a = 1.0;
		marker.color.b = 1.0;
		marker.type = 1; // CUBE
		marker.action = 0; // ADD
		return marker;
	}

	visualization_msgs::msg::Marker get_marker_from_part(Part part)
	{
		auto marker = visualization_msgs::msg::Marker();
		marker.header.stamp = rclcpp::Time();
		marker.pose.position.x = part.cx;
		marker.pose.position.y = part.cy;
		marker.scale.x = 1;
		marker.scale.y = 1;
		marker.color.a = 1.0;
		marker.color.g = 1.0;
		marker.type = 3; // CYLINDER
		marker.action = 0; // ADD
		return marker;
	}

	visualization_msgs::msg::Marker get_goal_marker(Part part)
	{
		auto marker = visualization_msgs::msg::Marker();
		marker.header.stamp = rclcpp::Time();
		marker.pose.position.x = part.cx;
		marker.pose.position.y = part.cy;
		marker.scale.x = 1;
		marker.scale.y = 1;
		marker.color.a = 1.0;
		marker.color.r = 1.0;
		marker.color.g = 1.0;
		marker.color.b = 1.0;
		marker.type = 1; // CUBE
		marker.action = 0; // ADD
		return marker;
	}

	int outstanding_responses_;
	std::string CONFIG_PATH_APPENDIX = "/configuration/products.yaml";
	std::string ORDER_PATH_APPENDIX = "/orders/";
	std::string file_path_;
	std::vector <Product> products_;
	std::mutex orders_mutex_;
	order_optimizer_interfaces::msg::NextOrder next_order_;
	std::vector <order_optimizer_interfaces::msg::Order> orders_;
	geometry_msgs::msg::PoseStamped current_position_;
	rclcpp_action::Client<OrderParse>::SharedPtr client_ptr_;
	rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr publisher_;
	rclcpp::Subscription<order_optimizer_interfaces::msg::NextOrder>::SharedPtr next_order_subscription_;
	rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr current_position_subscription_;
};

int main(int argc, char ** argv)
{
	rclcpp::init(argc, argv);
	rclcpp::spin(std::make_shared<OrderOptimizer>());
	rclcpp::shutdown();
	return 0;
}

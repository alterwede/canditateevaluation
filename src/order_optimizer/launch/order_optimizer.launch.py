from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    ld = LaunchDescription()
    order_optimizer_node = Node(
        package="order_optimizer",
        executable="order_optimizer_node",
        parameters=[
        	{"file_path": "/home/ubuntu/eclipse-workspace-cpp/CandidateEvaluation/sample_files"}
        ]
    )
    order_parse_node = Node(
        package="order_optimizer",
        executable="order_parse_server"
    )
    ld.add_action(order_parse_node)
    ld.add_action(order_optimizer_node)
    return ld
